import * as React from 'react';
import {useRef, useState} from 'react';
import 'semantic-ui-css/semantic.min.css';
import './SlidingColumnsTemplate.scss';



/**
 * Provides informaion about dragging status and width of each column to child elements.
 */
export const SlidingColumnContext = React.createContext({isDragging: false, columnWidth: 500});




interface PropsType
{
	left: React.ReactNode;
	right: React.ReactNode;
}



/**
 * Renders two colummns with a sliding separator between them.
 */
export default function SlidingColumnsTemplate({left, right}: PropsType)
{
	const mainDivRef = useRef<HTMLDivElement>();

	/**
	 * This flag indicates the status of dragging separator bar
	 */
	const [isDragging, setDragging] = useState(false);


	/**
	 * The current width of the developer panel.
	 * After dragging the shift value will be added to panelWidth
	 */
	const [rightColWidth, setRightColWidth] = useState(500);


	return (
	<div
		className = 'SlidingColumnsTemplate'
		ref = {mainDivRef}
		onMouseMove = {!isDragging ? undefined : (event) =>
		{
			if (isDragging)
			{
				event.preventDefault();

				if (mainDivRef.current)
				{
					setRightColWidth(mainDivRef.current.clientWidth - event.clientX);
				}
			}
		}}

		onMouseUp = {!isDragging ? undefined : (event) =>
		{
			if (isDragging)
			{
				event.preventDefault();
				setDragging(false);
			}
		}}
	>

		<div className = 'left'>
			<SlidingColumnContext.Provider value = {{
				isDragging,
				columnWidth: (
					mainDivRef.current ?
					mainDivRef.current.clientWidth - 10 - rightColWidth :
					window.document.body.clientWidth - 10 - rightColWidth
				),
			}}>
				{left}
			</SlidingColumnContext.Provider>
		</div>

		<div
			className = 'separator'
			onMouseDown = {isDragging ? undefined : (event) =>
			{
				event.preventDefault();
				setDragging(true);
			}}
		/>

		<div className = 'right' style = {{width: rightColWidth}}>
			<SlidingColumnContext.Provider value = {{
				isDragging,
				columnWidth: rightColWidth,
			}}>
				{ right }
			</SlidingColumnContext.Provider>
		</div>

	</div>
	);
}
