import * as React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from 'semantic-ui-react';
import './TwoButtonsTemplate.scss';




interface PropsType
{
	leftText: string;
	leftAction: any;
	leftLoading?: boolean;

	rightText: string;
	rightAction: any;
	rightLoading?: boolean;
}


export default function TwoButtonsTemplate({
	leftText, leftAction, leftLoading,
	rightText, rightAction, rightLoading,
}: PropsType)
{
	const dispatch = useDispatch();

	return (
	<div className = 'TwoButtonsTemplate'>
		<Button
			disabled = { leftAction === undefined }
			loading = {leftLoading}
			secondary size = 'big'
			onClick = {() => dispatch(leftAction)}
		>
			{leftText}
		</Button>

		<Button
			disabled = { rightAction === undefined }
			loading = {rightLoading}
			primary size = 'big'
			onClick = {() => dispatch(rightAction)}
		>
			{rightText}
		</Button>
	</div>
	);
}
