import * as React from 'react';
import { Header, Icon } from 'semantic-ui-react';
import * as CartAction from '../../store/cart/CartAction';
import PageTemplate from '../page-template/PageTemplate';
import TwoButtonsTemplate from '../two-buttons-template/TwoButtonsTemplate';
import './OrderCancellationPage.scss';




/**
 * This component will be displayed when cart.status === 'CANCELLING'.
 * It will warn user to return all the objects before cancelling the order and locking the fridge.
 */
export default function OrderCancellationPage()
{
	return (
		<PageTemplate className = 'OrderCancellationPage' width = {700}>

			<div className = 'message'>
				<Header as='h2'>
					<Icon name='hand point right outline'/>

					<Header.Content>
						Do you want to cancel your order?

						<Header.Subheader>
							Please return all the products to the fridge and press "Cancel Order" or
							continue your shopping by pressing "Continue Shopping..."
						</Header.Subheader>
					</Header.Content>
				</Header>
			</div>

			<TwoButtonsTemplate
				leftAction = { CartAction.ClearAll() }
				leftText = 'Cancel Order'
				rightAction = { CartAction.SetEditing() }
				rightText = 'Continue Shopping...'
			/>

		</PageTemplate>
	);
}
