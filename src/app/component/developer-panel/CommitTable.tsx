import * as React from 'react';
import { Button, Header, Icon, Table } from 'semantic-ui-react';
import ProductType from '../../store/type/ProductType';
import UserType from '../../store/type/UserType';




interface PropsType
{
	header: string;
	subheader: string;
	list: Array<UserType | ProductType>;
	icon: any;
	action: (value: any) => void;
}




export default function CommitTable({header, list, action, icon, subheader}: PropsType)
{
	return (
	<>
		<Header as='h4' inverted>
			<Icon name={icon} />
			<Header.Content>
				{header}
				<Header.Subheader>{subheader}</Header.Subheader>
			</Header.Content>
		</Header>

		<Table singleLine inverted compact fixed>
			<Table.Header>
				<Table.Row>
					<Table.HeaderCell>Name</Table.HeaderCell>
					<Table.HeaderCell textAlign = 'right'>Action</Table.HeaderCell>
				</Table.Row>
			</Table.Header>

			<Table.Body>
			{
				list.map((row) => (
					<Table.Row key = {row.guid}>

						<Table.Cell>
							{ row.name }
						</Table.Cell>

						<Table.Cell textAlign = 'right'>
							<Button
								size = 'mini'
								icon
								labelPosition='right'
								onClick = {() => action(row.guid)}
							>
								Commit <Icon name = 'upload' />
							</Button>
						</Table.Cell>

					</Table.Row>
				))
			}
			</Table.Body>
		</Table>
	</>);
}
