import * as React from 'react';
import { useDispatch } from 'react-redux';
import { connect } from 'react-redux';
import * as CartAction from '../../store/cart/CartAction';
import { AppStateType } from '../../store/CreateStore';
import CommitTable from './CommitTable';
import './DeveloperPanel.scss';




function DeveloperPanel({database}: Partial<AppStateType>)
{
	const dispatch = useDispatch();


	return (
		<div className = 'DeveloperPanel'>
			{
				database.status === 'FETCHED'
				&&
				<>
					<CommitTable
						header = 'Users'
						subheader = 'Simulates the skanning of RFID badges.'
						icon = 'users'
						list = {database.data.users}
						action = {(guid: string) => dispatch(CartAction.AddUser({guid}))}
					/>

					<CommitTable
						header = 'Products'
						subheader = 'Simulates the barcode reader.'
						icon = 'barcode'
						list = {database.data.products}
						action = { (guid: string) => dispatch(
							CartAction.AddProduct({guid, increment: 1}),
						)}
					/>
				</>
			}
		</div>
	);
}




export default connect((state: AppStateType) => (
{
	database: state.database,
}))(DeveloperPanel);
