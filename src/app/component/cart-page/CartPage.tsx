import * as React from 'react';
import { connect } from 'react-redux';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { Icon } from 'semantic-ui-react';
import { AppStateType } from '../../store/CreateStore';
import DatabaseType from '../../store/type/DatabaseType';
import PageTemplate from '../page-template/PageTemplate';
import './CartPage.scss';
import PaymentSection from './PaymentSection';
import ProductItem from './ProductItem';
import UserAvatar from './UserAvatar';




/**
 * Renders the active order (cart !== null)
 */
function CartPage({database, cart}: Partial<AppStateType>)
{
	return (
		<PageTemplate width = {700} className = 'CartPage'>

			<div className = 'users'>

				<div className = 'instruction'>
					{
						cart.userIds.length > 1
						?
						<><Icon name = 'user close'/> You can remove users by tapping on their avatars!</>
						:
						<><Icon name = 'user plus'/> Share the price by adding more users!</>
					}
				</div>

				<TransitionGroup className='avatars'>
				{
					cart.userIds.map((userId) => (
						<CSSTransition key = {userId} timeout={200} classNames='item'>
							<UserAvatar
								userId = {userId}
								users = {database.data.users}
								removable = {cart.userIds.length > 1}
							/>
						</CSSTransition>
					))
				}
				</TransitionGroup>
			</div>



			<div className = 'products'>
				<TransitionGroup className='ui divided items'>
				{
					cart.productItems.map((item) =>
					(
						<CSSTransition timeout={200} classNames='item' key = {item.guid}>
							<ProductItem
								item = {item}
								products = { database.data.products }
							/>
						</CSSTransition>
					))
				}
				</TransitionGroup>
			</div>


			<PaymentSection
				cart = {cart}
				database = {database as DatabaseType}
			/>

		</PageTemplate>
	);
}




export default connect((state: AppStateType) => (
{
	database: state.database,
	cart: state.cart,
}))(CartPage);
