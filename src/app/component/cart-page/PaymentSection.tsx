import * as React from 'react';
import * as CartAction from '../../store/cart/CartAction';
import DatabaseType from '../../store/type/DatabaseType';
import OrderType from '../../store/type/OrderType';
import TwoButtonsTemplate from '../two-buttons-template/TwoButtonsTemplate';




interface PropsType
{
	cart: OrderType;
	database: DatabaseType;
}




/**
 * PaymentSection will render the payment and cancelation buttons in CartPage.
 * This will also calculate the total price of the items.
 */
export default function PaymentSection({cart, database}: PropsType)
{
	let totalPrice = 0;

	cart.productItems.forEach((item) =>
	{
		const product = database.data.products.find((record) => record.guid === item.guid);
		totalPrice += item.quantity * product.price;
	});

	const usersCount = cart.userIds.length;


	return (
	<TwoButtonsTemplate
		leftAction = { CartAction.SetCancelling() }
		leftText = 'Cancel Shopping'

		rightAction = {
			totalPrice === 0
			?
			undefined
			:
			CartAction.AddToQueue()
		}

		rightText = {
			cart.userIds.length > 1
			?
			`Each Pays ${totalPrice} CHF / ${usersCount} = ${Math.floor((totalPrice / usersCount) * 100) / 100} CHF`
			:
			`Pay ${totalPrice} CHF`
		}

		rightLoading = {cart.status === 'QUEUEING'}
	/>
	);
}
