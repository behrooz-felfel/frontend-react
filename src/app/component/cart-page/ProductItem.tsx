import * as React from 'react';
import { useDispatch } from 'react-redux';
import { Button, Item } from 'semantic-ui-react';
import * as CartAction from '../../store/cart/CartAction';
import OrderType from '../../store/type/OrderType';
import ProductType from '../../store/type/ProductType';




interface PropsType
{
	item: OrderType['productItems'][0];
	products: ProductType[];
}




/**
 * A ProducItem renders an element into the list of products in CartPage.
 * This will provide a set of button for incrementing/decrementing quantity of this item by 1.
 */
export default function ProductItem({item, products}: PropsType)
{
	const dispatch = useDispatch();
	const {guid, quantity} = item;
	const {description, image, name, price} = products.find( (p) => p.guid === guid);

	return (
	<Item>

		<Item.Image size = 'tiny' src={image} />

		<Item.Content verticalAlign = 'middle'>
			<Item.Header>{name}</Item.Header>
			<Item.Meta>Quantity: {quantity} Price: {price} CHF</Item.Meta>

			<Item.Description>{description}</Item.Description>
			<Item.Extra></Item.Extra>
		</Item.Content>

		<Button.Group vertical size = 'small'>
			<Button icon = 'plus'
				onClick = { () => dispatch( CartAction.AddProduct({guid, increment: 1}) ) }
			/>
			{
				quantity > 1
				&&
				<Button icon = 'minus'
					onClick = { () => dispatch( CartAction.AddProduct({guid, increment: -1}) ) }
				/>
			}
			{
				quantity <= 1
				&&
				<Button icon = 'trash'
					onClick = { () => dispatch( CartAction.RemoveProduct({guid}) ) }
				/>
			}
		</Button.Group>
	</Item>
	);
}
