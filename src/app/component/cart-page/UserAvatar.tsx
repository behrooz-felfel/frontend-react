import * as React from 'react';
import { useDispatch } from 'react-redux';
import * as CartAction from '../../store/cart/CartAction';
import OrderType from '../../store/type/OrderType';
import UserType from '../../store/type/UserType';




interface PropsType
{
	userId: OrderType['userIds'][0];
	users: UserType[];
	removable: boolean;
}




export default function UserAvatar({userId, users, removable}: PropsType)
{
	const user = users.find((u) => u.guid === userId);
	const dispatch = useDispatch();

	return (
	<div
		className = 'UserAvatar'
		onClick = {() => (
			removable ?
			dispatch(CartAction.RemoveUser({guid: userId})) :
			undefined
		)}
	>
		<img
			src = { user.image }
			alt = { user.name }
		/>

		<h5>{ user.name }</h5>
	</div>
	);
}
