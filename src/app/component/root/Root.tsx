import * as React from 'react';
import { Provider } from 'react-redux';
import 'semantic-ui-css/semantic.min.css';
import {StoreType} from '../../store/CreateStore';
import DeveloperPanel from '../developer-panel/DeveloperPanel';
import SlidingColumnsTemplate from '../sliding-columns-template/SlidingColumnsTemplate';
import UserPanel from '../user-panel/UserPanel';




interface PropsType
{
	store: StoreType;
}




/**
 * The parent rendered component on screen
 */
export default function Root({store}: PropsType)
{
	return (
		<Provider store = {store}>
			<SlidingColumnsTemplate
				left = { <UserPanel /> }
				right = { <DeveloperPanel /> }
			/>
		</Provider>
	);
}
