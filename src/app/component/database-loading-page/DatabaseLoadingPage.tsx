import * as React from 'react';
import { Loader } from 'semantic-ui-react';
import PageTemplate from '../page-template/PageTemplate';




export default function DatabaseLoadingPage()
{
	return (
		<PageTemplate alignCenter>
			<Loader active inline='centered' content = 'Loading'/>
		</PageTemplate>
	);
}
