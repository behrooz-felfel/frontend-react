import * as React from 'react';
import { connect } from 'react-redux';
import { AppStateType } from '../../store/CreateStore';
import CartPage from '../cart-page/CartPage';
import DatabaseLoadingPage from '../database-loading-page/DatabaseLoadingPage';
import OrderCancellationPage from '../order-cancellation-page/OrderCancellationPage';
import WelcomePage from '../welcome-page/WelcomePage';
import * as CoverUrl from './cover.jpg';
import './UserPanel.scss';




/**
 * UserPanel will choose and render different pages.
 */
function UserPanel({database, cart}: Partial<AppStateType>)
{
	return (
		<div
			className = 'UserPanel'
			style = {{ backgroundImage: `url("${CoverUrl}")` }}
		>
			{
				// If the database has not been loaded yet
				(database.status !== 'FETCHED' && <DatabaseLoadingPage/>)
				||

				// If the user hasn't started the shopping process
				(cart === null && <WelcomePage />)
				||

				// If the cart is in cancellation state
				(cart.status === 'CANCELLING' && <OrderCancellationPage />)
				||

				// If user is in the shopping process
				<CartPage />
			}
		</div>
	);
}



export default connect((state: AppStateType) => (
{
	database: state.database,
	cart: state.cart,
}))(UserPanel);
