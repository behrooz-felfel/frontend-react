import * as React from 'react';
import './PageTemplate.scss';



interface Props
{
	width?: number | string;
	className?: string;
	alignCenter?: boolean;
	children: JSX.Element[] | JSX.Element;
}



const PageTemplate = React.forwardRef ((props: Props, ref: any) =>
{
	const {children, alignCenter} = props;
	let {width, className} = props;

	if (!width)
	{
		width = 500;
	}

	if (className === undefined)
	{
		className = '';
	}

	className += ' PageTemplate';

	return (
		<div
			className = {className}
			ref ={ref}
		>
			<div
				className = {'wrapper ' + (alignCenter ? 'align-center' : '')}
				style = {{maxWidth: width}}
			>
				{children}
			</div>
		</div>
	);
});


export default PageTemplate;
