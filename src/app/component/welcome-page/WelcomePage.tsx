import { useState } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import { Divider, Feed, Header, Icon } from 'semantic-ui-react';
import { AppStateType } from '../../store/CreateStore';
import PageTemplate from '../page-template/PageTemplate';
import {SlidingColumnContext} from '../sliding-columns-template/SlidingColumnsTemplate';
import './WelcomePage.scss';




/**
 * WelcomePage renders the scan badge message and latest news feed on the screen.
 * Maximum width of this page is 500 when the article is not open and 1000 when it is open.
 * To calculate the maximum width of the wrapper element width article element a REF object is passed to PageTemplate.
 */
function WelcomePage({news}: Partial<AppStateType>)
{
	const [activePostIndex, setActivePostIndex] = useState(-1);
	const [articleView, setArticleView] = useState(false);


	return (
	<SlidingColumnContext.Consumer>
	{({columnWidth, isDragging}) => {

		const totalWidth = Math.min(columnWidth, 1000);

		return (
			<PageTemplate
				className = {`WelcomePage ${isDragging ? 'dragging' : 'not-dragging'}`}
				width = {articleView ? totalWidth : totalWidth / 2}
			>
				<div
					className = 'col num-0'
					style = {{width: totalWidth / 2}}
				>

					<div className = 'row num-0'>
						<Header as='h2' icon>
							<Icon name='id badge' />
							Scan your FELFEL Badge!
							<Header.Subheader>
								Scan your personal badge with RFID reader.
							</Header.Subheader>
						</Header>
					</div>

					{
						news.status === 'FETCHED'
						&&
						<Feed className = 'row num-1'>
							<Divider horizontal as = 'i' style = {{fontWeight: 100}}>
								Latest FELFEL News
							</Divider>

							{
								news.data.map((post, postIndex) => (
								<Feed.Event
									key = {post.id}
									onClick = {() =>
									{
										setArticleView(postIndex !== activePostIndex || !articleView);
										setActivePostIndex(postIndex);
									}}
									style = {
									{
										opacity: (activePostIndex === -1 || !articleView || activePostIndex === postIndex) ? 1 : 0.5,
									}}
								>
									<Feed.Content>

										<Feed.Summary>
											{ post.title.rendered }
											<Feed.Date>{ post.date }</Feed.Date>
										</Feed.Summary>

									</Feed.Content>
								</Feed.Event>
								))
							}
						</Feed>
					}
				</div>

				<div
					className = 'col num-1'
					style = {{width: totalWidth / 2, left: totalWidth / 2}}
				>
					{
						activePostIndex !== -1
						&&
						<article>
							<h2>{ news.data[activePostIndex].title.rendered }</h2>
							<div dangerouslySetInnerHTML = {{__html: news.data[activePostIndex].content.rendered}}/>
						</article>
					}
				</div>

			</PageTemplate>
		);
	}}
	</SlidingColumnContext.Consumer>
	);
}


export default connect((state: AppStateType) => ({
	news: state.news,
}))(WelcomePage);
