import React from 'react';
import ReactDOM from 'react-dom';
import Root from '../component/root/Root';
import CreateStore from '../store/CreateStore';
import FetchData from './FetchData';




/**
 * This function bootstraps all of the initial required functionalities.
 */
export default function LaunchApp()
{
	const store = CreateStore();

	ReactDOM.render(
		<Root store = {store} />,
		document.getElementById('root'),
	);

	FetchData(store);
}
