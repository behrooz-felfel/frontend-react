import Sleep from '../common/Sleep';
import {StoreType} from '../store/CreateStore';
import * as DatabseAction from '../store/database/DatabaseAction';
import * as ImageAction from '../store/image/ImageAction';
import * as NewsAction from '../store/news/NewsAction';




/**
 * The promise will be resolved when the shopping cart is empty (cart === null).
 * This function can postpone the execution of less important functionalities during the shopping process.
 */
async function WaitForEmptyCart(store: StoreType)
{
	if (store.getState().cart === null)
	{
		return true;
	}
	else
	{
		return new Promise((resolve) =>
		{
			store.subscribe(() =>
			{
				if (store.getState().cart === null)
				{
					resolve(true);
				}
			});
		});
	}
}




/**
 * Fetch the initial database data and preload News and Images data.
 */
export default async function FetchData(store: StoreType)
{
	// Load database.json
	await store.dispatch(DatabseAction.Fetch());

	// Load wp-json content
	await store.dispatch(NewsAction.Fetch());

	// Extract all of the images
	const state = store.getState();

	const images = [] as string[];
	const {users, products} = state.database.data;

	users.forEach((user) => images.push(user.image));
	products.forEach((product) => images.push(product.image));

	// Load all of the images and push to the redux state
	for (const src of images)
	{
		await WaitForEmptyCart(store);
		await store.dispatch(ImageAction.Load({src}));
		await Sleep(500);
	}
}
