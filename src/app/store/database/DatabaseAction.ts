import Sleep from '../../common/Sleep';
import { AppStateType } from '../CreateStore';
import DatabaseType from '../type/DatabaseType';




export function SetFetching()
{
	return {
		type: 'DATABASE_SET_FETCHING' as 'DATABASE_SET_FETCHING',
	};
}




export function SetError(payload: {message: string})
{
	return {
		type: 'DATABASE_SET_ERROR' as 'DATABASE_SET_ERROR',
		payload,
	};
}




export function SetFetched(payload: DatabaseType['data'])
{
	return {
		type: 'DATABASE_SET_FETCHED' as 'DATABASE_SET_FETCHED',
		payload,
	};
}




export function Fetch(): any
{
	return async (dispatch: (action: any) => void, getState: () => AppStateType) =>
	{
		if (getState().database.status !== 'FETCHING')
		{
			dispatch(SetFetching());

			await Sleep(1000);

			try
			{
				const response: Response = await fetch('database.json');

				if (response.ok)
				{
					const data: any = await response.json();
					dispatch(SetFetched(data));
					return true;
				}
				else
				{
					dispatch(SetError({message: 'Unknown'}));
					return false;
				}
			}
			catch (error)
			{
				dispatch(SetError({message: error.message}));
				return false;
			}
		}
		else
		{
			return true;
		}
	};
}
