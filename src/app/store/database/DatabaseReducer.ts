import DatabaseType from '../type/DatabaseType';
import { SetError, SetFetched, SetFetching } from './DatabaseAction';




type ActionTypes = ReturnType<
	typeof SetError		|
	typeof SetFetched		|
	typeof SetFetching
>;


const initialState: DatabaseType = {
	status: 'NONE',
	updated: -1,
	error: null,
	data: null,
};


export default function DatabaseReducer(state = initialState, action: ActionTypes): DatabaseType
{
	switch (action.type)
	{
		case 'DATABASE_SET_FETCHING':
			if (state.status !== 'FETCHING')
			{
				return {
					status: 'FETCHING' as 'FETCHING',
					updated: Date.now(),
					data: undefined,
					error: undefined,
				};
			}
			return state;

		case 'DATABASE_SET_FETCHED':
			return {
				status: 'FETCHED' as 'FETCHED',
				updated: Date.now(),
				error: null,
				data: action.payload,
			};

		case 'DATABASE_SET_ERROR':
			return {
				status: 'ERROR' as 'ERROR',
				updated: Date.now(),
				error: action.payload.message,
				data: undefined,
			};

		default:
			return state;
	}
}
