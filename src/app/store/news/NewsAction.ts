import RemoveWpReadMore from '../../common/RemoveWpReadMore';
import RemoveWpShortcodes from '../../common/RemoveWpShortcodes';
import ReplaceAnchorsWithText from '../../common/ReplaceAnchorsWithText';
import Sleep from '../../common/Sleep';
import { AppStateType } from '../CreateStore';
import NewsType from '../type/NewsType';
import WpPostType from '../type/WpPostType';




/**
 * Indicates the beginning of news feed fetching process.
 */
export function SetFetching()
{
	return {
		type: 'NEWS_SET_FETCHING' as 'NEWS_SET_FETCHING',
	};
}




/**
 * Indicates the error in fetching process.
 */
export function SetError(payload: {message: string})
{
	return {
		type: 'NEWS_SET_ERROR' as 'NEWS_SET_ERROR',
		payload,
	};
}




/**
 * Indicates the completion of fetching process.
 */
export function SetFetched(payload: NewsType['data'])
{
	return {
		type: 'NEWS_SET_FETCHED' as 'NEWS_SET_FETCHED',
		payload,
	};
}




/**
 * Send the async fetch request only if it has not been send before.
 * The function will sanitise the fetched data after completion by:
 * - Removing wordpress shortcodes
 * - Removing read more link
 * - Replacing all of the links with their inner contents.
 *
 * @see: NewsController
 */
export function Fetch(): any
{
	return async (dispatch: (action: any) => void, getState: () => AppStateType) =>
	{
		if (getState().news.status !== 'FETCHING')
		{
			dispatch(SetFetching());

			await Sleep(1000);

			try
			{
				const response: Response = await fetch('https://felfel.ch/en/wp-json/wp/v2/posts?per_page=4');

				if (response.ok)
				{
					const data: WpPostType[] = await response.json();

					const sanitiseRendered = (record: {rendered: string}) =>
					{
						return {
							rendered: ReplaceAnchorsWithText(
								RemoveWpShortcodes(
									RemoveWpReadMore(record.rendered),
								),
							),
						};
					};

					const sanitisePost = (post: WpPostType) => (
					{
						...post,
						excerpt: sanitiseRendered(post.excerpt),
						content: sanitiseRendered(post.content),
					} as WpPostType);

					dispatch(
						SetFetched(
							data.map( (post) => sanitisePost(post) ),
						),
					);

					return true;
				}
				else
				{
					dispatch(SetError({message: 'Unknown'}));
					return false;
				}
			}
			catch (error)
			{
				dispatch(SetError({message: error.message}));
				return false;
			}
		}
	};
}
