import NewsType from '../type/NewsType';
import { SetError, SetFetched, SetFetching } from './NewsAction';




type ActionTypes = ReturnType<
	typeof SetError		|
	typeof SetFetched		|
	typeof SetFetching
>;


const initialState: NewsType = {
	status: 'NONE',
	updated: -1,
	data: [],
};


export default function NewsReducer(state = initialState, action: ActionTypes): NewsType
{
	switch (action.type)
	{
		case 'NEWS_SET_FETCHING':
			if (state.status !== 'FETCHING')
			{
				return {
					status: 'FETCHING' as 'FETCHING',
					updated: Date.now(),
					data: [],
				};
			}
			return state;


		case 'NEWS_SET_FETCHED':
			return {
				status: 'FETCHED' as 'FETCHED',
				updated: Date.now(),
				data: action.payload,
			};


		case 'NEWS_SET_ERROR':
			return {
				...state,
				status: 'ERROR',
			};


		default:
			return state;
	}
}
