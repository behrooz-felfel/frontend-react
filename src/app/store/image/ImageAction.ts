import LoadImage from '../../common/LoadImage';




/**
 * Add the src to the list of loaded images.
 */
export function SetLoaded(payload: {src: string})
{
	return {
		type: 'SET_LOADED' as 'SET_LOADED',
		payload,
	};
}




export function Load(payload: {src: string}): any
{
	return async (dispatch: any) =>
	{
		await LoadImage(payload.src);
		dispatch(SetLoaded(payload));
		return true;
	};
}
