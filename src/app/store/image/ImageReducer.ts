import { SetLoaded } from './ImageAction';



type ActionTypes = ReturnType<
	typeof SetLoaded
>;


const initialState = [] as string[];


export default function ImageReducer(state = initialState, action: ActionTypes): typeof initialState
{
	switch (action.type)
	{
		case 'SET_LOADED':
			if (!state.includes(action.payload.src))
			{
				return [...state, action.payload.src];
			}

			return state;

		default:
			return state;
	}
}
