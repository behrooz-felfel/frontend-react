import uuid from 'uuid/v1';
import OrderType from '../type/OrderType';
import {
	AddProduct,
	AddUser,
	ClearAll,
	ClearProducts,
	RemoveProduct,
	RemoveUser,
	SetApproving,
	SetCancelling,
	SetEditing,
	SetQueueing,
} from './CartAction';




type ActionTypes = ReturnType<
	typeof AddUser			|
	typeof RemoveUser		|
	typeof ClearAll		|
	typeof AddProduct		|
	typeof RemoveProduct	|
	typeof ClearProducts	|
	typeof SetQueueing	|
	typeof SetApproving	|
	typeof SetCancelling	|
	typeof SetEditing
>;


const initialState: OrderType = null;


export default function DatabaseReducer(state = initialState, action: ActionTypes): OrderType
{
	switch (action.type)
	{
		case 'CART_ADD_USER':
			if (state === null)
			{
				return {
					guid: uuid(),
					status: 'EDITING',
					productItems: [],
					userIds: [action.payload.guid],
				};
			}
			else if (!state.userIds.includes(action.payload.guid))
			{
				return {
					...state,
					status: 'EDITING',
					userIds: [...state.userIds, action.payload.guid],
				};
			}

			return {
				...state,
				status: 'EDITING',
			};


		case 'CART_REMOVE_USER':
			if (state !== null && state.userIds.includes(action.payload.guid))
			{
				return {
					...state,
					userIds: state.userIds.filter((guid) => guid !== action.payload.guid),
				};
			}
			return state;


		case 'CART_CLEAR_ALL':
			return null;


		case 'CART_ADD_PRODUCT':
			if (state !== null)
			{
				const productIndex = state.productItems.findIndex((product) => product.guid === action.payload.guid);

				if (productIndex === -1)
				{
					return {
						...state,
						status: 'EDITING',
						productItems: [
							...state.productItems,
							{
								guid: action.payload.guid,
								quantity: action.payload.increment,
							},
						],
					};
				}
				else
				{
					state.productItems[productIndex].quantity += action.payload.increment;
					return {
						...state,
						status: 'EDITING',
					};
				}
			}

			return state;


		case 'CART_REMOVE_PRODUCT':
			return {
				...state,
				productItems: state.productItems.filter((product) => product.guid !== action.payload.guid),
			};


		case 'CART_CLEAR_PRODUCTS':
			return {
				...state,
				productItems: [],
			};


		case 'CART_SET_QUEUEING':
			return {
				...state,
				status: 'QUEUEING',
			};


		case 'CART_SET_APPROVING':
			return {
				...state,
				status: 'APPROVING',
			};


		case 'CART_SET_CANCELLING':
			return {
				...state,
				status: 'CANCELLING',
			};


		case 'CART_SET_EDITING':
			return {
				...state,
				status: 'EDITING',
			};


		default:
			return state;
	}
}
