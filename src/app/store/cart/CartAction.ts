import Sleep from '../../common/Sleep';
import { AppStateType } from '../CreateStore';



/**
 * Creating a new cart if there is no active cart in the store and
 * Adding the User (payload.guid) to the active cart
 */
export function AddUser( payload: {guid: string} )
{
	return {
		type: 'CART_ADD_USER' as 'CART_ADD_USER',
		payload,
	};
}




export function RemoveUser(payload: {guid: string})
{
	return {
		type: 'CART_REMOVE_USER' as 'CART_REMOVE_USER',
		payload,
	};
}




export function ClearAll()
{
	return {
		type: 'CART_CLEAR_ALL' as 'CART_CLEAR_ALL',
	};
}




export function AddProduct(payload: {guid: string, increment: number})
{
	return {
		type: 'CART_ADD_PRODUCT' as 'CART_ADD_PRODUCT',
		payload,
	};
}




export function RemoveProduct(payload: {guid: string})
{
	return {
		type: 'CART_REMOVE_PRODUCT' as 'CART_REMOVE_PRODUCT',
		payload,
	};
}




export function ClearProducts()
{
	return {
		type: 'CART_CLEAR_PRODUCTS' as 'CART_CLEAR_PRODUCTS',
	};
}




export function SetQueueing()
{
	return {
		type: 'CART_SET_QUEUEING' as 'CART_SET_QUEUEING',
	};
}




export function SetCancelling()
{
	return {
		type: 'CART_SET_CANCELLING' as 'CART_SET_CANCELLING',
	};
}




export function SetApproving()
{
	return {
		type: 'CART_SET_APPROVING' as 'CART_SET_APPROVING',
	};
}




export function SetEditing()
{
	return {
		type: 'CART_SET_EDITING' as 'CART_SET_EDITING',
	};
}




export function AddToQueue()
{
	return async (dispatch: (action: any) => void, getState: () => AppStateType) =>
	{
		if (getState().cart.status !== 'QUEUEING')
		{
			dispatch(SetQueueing());

			// Simulating Validation and Queueing
			await Sleep(500);



			dispatch(ClearAll());
		}
	};
}
