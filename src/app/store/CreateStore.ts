import { applyMiddleware, combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import CartReducer from './cart/CartReducer';
import DatabaseReducer from './database/DatabaseReducer';
import ImageReducer from './image/ImageReducer';
import NewsReducer from './news/NewsReducer';




/**
 * Combines all of the reducers with redux-devtool and thunk middleware.
 */
export default function CreateStore()
{
	const reducers = combineReducers(
	{
		// List of all of the loaded images
		image: ImageReducer,

		// Status of database last fetch and its data
		database: DatabaseReducer,

		// The active order (shopping cart)
		cart: CartReducer,

		// The status and data of lastest news
		news: NewsReducer,
	});


	const store = createStore(
		reducers,
		composeWithDevTools(
			applyMiddleware(thunk),
		),
	);

	return store;
}


/**
 * Typestript definition for redux store.
 */
export type StoreType = ReturnType<typeof CreateStore>;


/**
 * Typescript definition for the state of the store.
 */
export type AppStateType = ReturnType<StoreType['getState']>;
