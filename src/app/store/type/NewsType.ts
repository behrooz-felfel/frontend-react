import WpPostType from './WpPostType';




export default interface NewsType
{
	/** The current state of fetching latest news process */
	status: 'NONE' | 'FETCHING' | 'ERROR' | 'FETCHED';

	/** The timestamp of latest news feed update */
	updated: number;

	/** The fetched wordpress articles */
	data: WpPostType[];
}
