export default interface NewsArticleType
{
	id: number;
	title: {rendered: string};
	content: {rendered: string};
	excerpt: {rendered: string};
	date: string;
}
