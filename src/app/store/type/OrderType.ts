import ProductType from './ProductType';
import UserType from './UserType';




/**
 * This type represent a payment order. This type with be used in cart reducer.
 * Each order holds reference to more than one user for co-paymentand a set of products and their ordered quantity.
 */
export default interface OrderType
{
	/** The order's id */
	guid: string;


	/**
	 * The status of the order
	 *
	 * - EDITING is when the shopping cart is active and users can add product to the order.
	 * - CANCELING is when users want to drop an order.
	 * - APROVING happens before users save the order and approve the payment.
	 * - QUEUEING happens after users accept the payment and to validate the request.
	 */
	status: 'EDITING' | 'CANCELLING' | 'APPROVING' | 'QUEUEING';


	/**
	 * The id of the added users
	 *
	 * @see DatabaseType.users
	 */
	userIds: Array<UserType['guid']>;


	/**
	 * The guid and the quantity of each added product
	 *
	 * @see DatabaseType.products
	 */
	productItems: Array<{ guid: ProductType['guid'], quantity: number }>;
}
