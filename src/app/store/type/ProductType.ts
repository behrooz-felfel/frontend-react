export default interface ProductType
{
	guid: string;
	name: string;
	description: string;
	image: string;
	price: number;
}