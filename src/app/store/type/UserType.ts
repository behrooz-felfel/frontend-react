export default interface UserType
{
	guid: string;
	image: string;
	name: string;
}
