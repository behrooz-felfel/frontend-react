import ProductType from './ProductType';
import UserType from './UserType';




export default interface DatabaseType
{
	status: 'NONE' | 'FETCHING' | 'ERROR' | 'FETCHED';
	updated: number;
	error: string;

	data: {
		users: UserType[];
		products: ProductType[];
	};
}
