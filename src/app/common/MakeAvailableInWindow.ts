/**
 * Set the value of a window property.
 * This is useful to expose a value in browser.
 * @param name Key
 * @param target Target value to be set
 */
export default function MakeAvailableInWindow(name: string, target: any)
{
	(window as any)[name] = target;
}
