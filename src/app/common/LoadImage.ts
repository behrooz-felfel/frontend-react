/**
 * Load an image in an async manner.
 *
 * example: await LoadImage("http://foo.bar/image.jpg")
 *
 * @param src Image source url
 */
export default function LoadImage(src: string)
{
	return new Promise((resolve, reject) =>
	{
		const img = new Image();
		img.onload = () => resolve(img);
		img.src = src;
	});
}
