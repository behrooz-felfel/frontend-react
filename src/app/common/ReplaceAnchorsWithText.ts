/**
 * Remove anchors and replace them with plain text.
 */
export default function ReplaceAnchorsWithText(input: string)
{
	const regex = /<\/?a[^>]*?>/gm;

	return input.replace(regex, '');
}
