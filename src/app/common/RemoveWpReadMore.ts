/**
 * Remove wordpress "Read more" link from post content.
 */
export default function RemoveWpReadMore(input: string)
{
	const regex = /<a[^>]+>Read More<\/a>/gm;

	return input.replace(regex, '');
}
