import RemoveWpShortcodes from "./RemoveWpShortcodes";
import RemoveWpReadMore from "./RemoveWpReadMore";
import ReplaceAnchorsWithText from "./ReplaceAnchorsWithText";




describe("Wordpress Post Sanitation", () =>
{
	test("Remove Shortcodes", () =>
	{
		const input = "<p>[a][a b] abc 123 [][/]";
	
		expect(RemoveWpShortcodes(input)).toBe("<p> abc 123 []");
	})

	test("Remove Read More", () =>
	{
		const input = "this is the story<a href=\"fake.ch\">Read More</a></p> ";
	
		expect(RemoveWpReadMore(input)).toBe("this is the story</p> ");
	})

	test("Replace anchors with text", () =>
	{
		const input = `what is going <a>on <b>HERE!</b></a>. <a href="fake.ch">I do not KNOW!</a>`;
	
		expect(ReplaceAnchorsWithText(input)).toBe(`what is going on <b>HERE!</b>. I do not KNOW!`);
	})
});