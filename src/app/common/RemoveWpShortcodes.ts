/**
 * Remove wordpress Shortcodes from post content.
 */
export default function RemoveWpShortcodes(input: string)
{
	const regex = /\[[^\]]+\]/gm;

	return input.replace(regex, '');
}
