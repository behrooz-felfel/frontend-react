# FELFEL Frontend Task

## Installation
Please run `npm install` to install all the required node_modules. To start the development server run `npm start` in the porject's root directory.

## Dependencies:
- react
- redux
- react-redux
- react-transition-group
- redux-thunk
- semantic-ui-react

## Alternative
based on convention these syntax can be used. I tried to show my skills in using hooks and other function in this project.
- useSelector & connect
- useContext & context.Consumer


## Gitlab CI
A CI pipeline will be triggered after each commit to master branch which will build the react application and expose the build folder. The built version is available at:
[https://behrooz-felfel.gitlab.io/frontend-react](https://behrooz-felfel.gitlab.io/frontend-react)